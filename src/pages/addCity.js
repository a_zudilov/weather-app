import React from "react";
import { push } from "react-router-redux";
import { connect} from "react-redux";
import AddCityComponent from "../components/addCity";
import DisplayCitiesComponent from "../components/displayCities";

export class AddCityPage extends React.Component {
    render () {
        return (
            <React.Fragment>
                <div className="text-center">
                    <div className="mt-5 mb-2">Введите название города</div>
                    <div className="input-group justify-content-center">
                        <AddCityComponent needSaveDefaul={true}/>
                    </div>
                    <div className="mt-2">
                        <DisplayCitiesComponent
                            needSaveDefaul={true}
                            checkState={ () => { this.props.changePage( "/" ) } }
                        />
                    </div>
                </div>
            </React.Fragment>
        )
    }
}

const mapDispatchToProps = ( dispatch ) => {
    return{
        changePage: ( path ) => dispatch( push( path ) ),
    }
};

export default connect( null, mapDispatchToProps )( AddCityPage );